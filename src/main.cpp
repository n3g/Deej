#include <Adafruit_NeoPixel.h>

const int NUM_SLIDERS = 6;
const int analogInputs[NUM_SLIDERS] = {A0, A1, A2, A3, A4, A5};

// MUTE LEDS
const int PIN = 8;
const int N_LEDS = 6;
Adafruit_NeoPixel muteStrip = Adafruit_NeoPixel(N_LEDS, PIN, NEO_GRB + NEO_KHZ800);

// BOTTOM LED STRIPS
const int STRIP_PIN = 9;
const int STRIP_N_LEDS = 16;
Adafruit_NeoPixel bottomStrips = Adafruit_NeoPixel(STRIP_N_LEDS, STRIP_PIN, NEO_GRB + NEO_KHZ800);

// SLIDERS VALUES
int analogSliderValues[NUM_SLIDERS];

// COLOR WHEEL
float brightness = 1;
uint32_t strip_state = 0;

uint32_t Wheel(Adafruit_NeoPixel *bottomStrips, byte WheelPos)
{
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85)
  {
    return bottomStrips->Color((255 - WheelPos * 3) * brightness, 0, WheelPos * 3 * brightness);
  }
  if (WheelPos < 170)
  {
    WheelPos -= 85;
    return bottomStrips->Color(0, WheelPos * 3 * brightness, (255 - WheelPos * 3) * brightness);
  }
  WheelPos -= 170;
  return bottomStrips->Color(WheelPos * 3 * brightness, (255 - WheelPos * 3) * brightness, 0);
}

// LED STRIP EFFECT - FADE ALL COLORS
void fade_color()
{
  for (unsigned int i = 0; i < bottomStrips.numPixels(); i++)
  {
    bottomStrips.setPixelColor(i, Wheel(&bottomStrips, strip_state));
  }
  bottomStrips.show();
  strip_state++;
  if (strip_state > 255)
    strip_state = 0;
}

// LED STRIP EFFECT - BASED ON MASTER VALUE
void fade_colorBasedOnMaster()
{
  for (unsigned int i = 0; i < bottomStrips.numPixels(); i++)
  {
    bottomStrips.setPixelColor(i, Wheel(&bottomStrips, map((float)analogRead(analogInputs[0]), 0, 1024, 0, 255)));
  }
  bottomStrips.show();
}

// LED STRIP EFFECT - MOVING RAINBOW
void moving_rainbow()
{
  for (unsigned int i = 0; i < bottomStrips.numPixels(); i++)
  {
    bottomStrips.setPixelColor(i, Wheel(&bottomStrips, ((i * 256 / bottomStrips.numPixels()) + strip_state) & 255));
  }
  bottomStrips.show();
  strip_state++;
}

// LED STRIP EFFECT - FILL COLOR
void fill_color(uint32_t color)
{
  byte r = ((color & 0xFF0000) >> 16) * brightness;
  byte g = ((color & 0xFF00) >> 8) * brightness;
  byte b = (color & 0xFF) * brightness;
  color = bottomStrips.Color(r, g, b);
  for (unsigned int i = 0; i < bottomStrips.numPixels(); i++)
  {
    bottomStrips.setPixelColor(i, color);
  }
  bottomStrips.show();
}

// LED STRIP EFFECT - HANDLE DYNAMIC BRIGHTNESS BASED ON MASTER VALUE
void dynamicBrightness()
{

  brightness = (float)analogRead(analogInputs[0]) / 1023;
}

void updateMuteLedState()
{
  for (int i = 0; i < NUM_SLIDERS; i++)
  {
    if (analogSliderValues[i] < 15) // tolerance to avoid blink
    {
      muteStrip.setPixelColor(i, 0, 0, 0);
      muteStrip.show();
    }
    else
    {
      muteStrip.setPixelColor(i, 0, 15, 15);
      muteStrip.show();
    }
  }
}

void updateSliderValues()
{
  for (int i = 0; i < NUM_SLIDERS; i++)
  {
    analogSliderValues[i] = analogRead(analogInputs[i]);
  }
}

void sendSliderValues()
{
  String builtString = String("");

  for (int i = 0; i < NUM_SLIDERS; i++)
  {
    builtString += String((int)analogSliderValues[i]);

    if (i < NUM_SLIDERS - 1)
    {
      builtString += String("|");
    }
  }

  Serial.println(builtString);
}

void printSliderValues()
{
  for (int i = 0; i < NUM_SLIDERS; i++)
  {
    String printedString = String("Slider #") + String(i + 1) + String(": ") + String(analogSliderValues[i]) + String(" mV");
    Serial.write(printedString.c_str());

    if (i < NUM_SLIDERS - 1)
    {
      Serial.write(" | ");
    }
    else
    {
      Serial.write("\n");
    }
  }
}

void setup()
{
  // SLIDERS
  for (int i = 0; i < NUM_SLIDERS; i++)
  {
    pinMode(analogInputs[i], INPUT);
  }

  // MUTE LED
  muteStrip.begin();

  // LAUNCH SEQUENCE
  int wait = 500;
  muteStrip.setPixelColor(0, 23, 2, 24);
  muteStrip.setPixelColor(5, 23, 2, 24);
  muteStrip.show();
  delay(wait);

  muteStrip.setPixelColor(1, 23, 2, 24);
  muteStrip.setPixelColor(4, 23, 2, 24);
  muteStrip.show();
  delay(wait);

  muteStrip.setPixelColor(2, 23, 2, 24);
  muteStrip.setPixelColor(3, 23, 2, 24);
  muteStrip.show();
  delay(wait);

  // BOTTOM LEDS
  bottomStrips.begin();
  bottomStrips.show();

  // SERIAL
  Serial.begin(9600);
}

void loop()
{

  // BOTTOM LIGHTNING EFFECT
  dynamicBrightness();
  moving_rainbow();
  //fade_color();
  //fade_colorBasedOnMaster();
  //fill_color(0xFF00FB); // Code Hexa eg. 0xFF00FB for pink

  // DEEJ
  updateSliderValues();
  updateMuteLedState();
  sendSliderValues();

  delay(10);
}